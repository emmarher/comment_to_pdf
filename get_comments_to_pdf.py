#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 16:02:41 2021

@author: emmarher
"""

# para hacer celdas de código #%%


#import json
import pandas as pd
import requests
import numpy as np
#from docxtpl import DocxTemplate as dxt


#Set the request parameters
url_tickets = 'https://cidesi.zendesk.com/api/v2/tickets.json'
url_users = 'https://cidesi.zendesk.com/api/v2/users.json'
url_comment = 'https://cidesi.zendesk.com/api/v2/tickets/{0}/comments.json'
email ='emmanuel.martinez@cidesi.edu.mx'
user = email + '/token'
# api_token = 'nozH1u9140lkCivkH0uZ5AkfGGdyYC5UCB2zfgJG'
api_token = '2SQPIgPn4cqNOKQ5ocjxUTIP6XS801AeiiNvN8Mz'

ticket_id =102


# get_comment = f'https://cidesi.zendesk.com/api/v2/tickets/{ticket_id}/comments.json'
get_comment = url_comment.format(ticket_id)

#Do the HTTP get request
response_tickets = requests.get(url_tickets, auth=(user,api_token))
response_comment = requests.get(get_comment, auth=(user,api_token))
response_users = requests.get(url_users, auth=(user,api_token))

def check_conection(response):
#Check for HTTP codes other than 200    

    if response.status_code != 200:
        print('Status: ',response.status_code, 'Problema with the request. Exiting')
    
        exit()

check_conection(response_tickets)
check_conection(response_comment)
check_conection(response_users)

#decode the JSOn response into a dictionary and use the data
data = response_tickets.json()
# data = response_tickets.content
items = data.items()

df = pd.DataFrame.from_dict(data)
df2 = pd.DataFrame.from_dict(data['tickets'])
df2 = df2.drop(['url','external_id', 'type','organization_id','collaborator_ids','follower_ids','email_cc_ids','forum_topic_id','problem_id','has_incidents', 'due_at', 'sharing_agreement_ids','followup_ids', 'allow_channelback'], axis=1)




# ==================  Download User data
#data_comment["comments"][1]["body"]
data_users = response_users.json()
list_key_data_user = ["id","name","role"]
list_data_user=[] # Storage id, name, role for each user


#data_users["users"][0]["name"]

for uc in range(data_users["count"]):
    tmp_list = []
    for lkdu in (list_key_data_user):
        tmp_list.append(data_users["users"][uc][lkdu])
    
    list_data_user.append(tmp_list)
        





# ========== Download Comment data, just one
data_comment = response_comment.json()
mess = ""
list_idUser_comments = []
for m in range(data_comment["count"]):    
    mess = mess+" "+ str(data_comment["comments"][m]["author_id"])+" : "+data_comment["comments"][m]["body"] +"\n"
    list_idUser_comments.append(data_comment["comments"][m]["author_id"])

# Unique id user in list_idUser_comments
uniq_idUser = np.unique(list_idUser_comments)

matrix_dat_user = np.array(list_data_user)



def where_id(uniq,matrix,ls_user):
    
    kk = []
    names_in_comment=[]
    
    for ui in range(len(uniq)):
#        print(ui)
#        print(np.where(np.array(matrix_dat_user[:,0])==str(uniq[ui])))
        kk.append(np.where(np.array(matrix_dat_user[:,0])==str(uniq[ui])))
        kk[ui]=kk[ui][0][0]
        names_in_comment.append(ls_user[kk[ui]][1])      
#        print(ls_user[kk[ui]][1])
        

    return names_in_comment
        
    
        
names_in_comment = where_id(uniq_idUser,matrix_dat_user,list_data_user)


#print(np.where(np.array(matrix_dat_user[:,0])==str(uniq_idUser[0])))

matrix_dat_user[1][0]


list_data_user[0]


# Replace id User in message (mess)
for ui in range(len(uniq_idUser)):
   mess = mess.replace(str(uniq_idUser[ui]),names_in_comment[ui])


print(mess)
# ======================== save raw data to csv
#df2.to_csv("raw_data.csv", encoding='utf-8-sig')






